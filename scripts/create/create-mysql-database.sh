#!/bin/bash

##
# DOCKER
##

docker exec -i $MARIADB_CONTAINER bash -c "mysql -u root -p$MYSQL_ROOT_PASSWORD -e \"CREATE DATABASE IF NOT EXISTS $MYSQL_DATABASE CHARACTER SET = '$MYSQL_CHARACTER' COLLATE = '$MYSQL_COLLATION';\""
