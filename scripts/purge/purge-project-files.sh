#!/bin/bash

##
# NGINX
##

# Purge files

rm $NGINX_DIRECTORY_CONFIGS/*
rm $NGINX_DIRECTORY_HOSTS/*

# Feedback

echo 'Purge nginx files ... done'

##
# APACHE
##

# Purge files

rm $APACHE_DIRECTORY_CONFIGS/*
rm $APACHE_DIRECTORY_HOSTS/*

# Feedback

echo 'Purge apache files ... done'

##
# MARIADB
##

# Purge files

rm $MARIADB_DIRECTORY_CONFIGS/*
rm -rf $MARIADB_DIRECTORY_DATABASES/
mkdir -p $MARIADB_DIRECTORY_DATABASES

# Feedback

echo 'Purge mariadb files ... done'

##
# PHPMYADMIN
##

# Purge files

rm $PHPMYADMIN_DIRECTORY_CONFIGS/*
rm $PHPMYADMIN_DIRECTORY_HOSTS/*

# Feedback

echo 'Purge phpmyadmin files ... done'
