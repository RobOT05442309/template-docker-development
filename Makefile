.PHONY: clean create destroy purge rebuild reset

clean:
	cd ./scripts/ && bash ./clean-project.sh

create:
	chmod -R +x ./scripts/. && cd ./scripts/ && bash ./create-project.sh

destroy:
	cd ./scripts/ && bash ./destroy-project.sh

purge:
	cd ./scripts/ && bash ./purge-project.sh

rebuild:
	cd ./scripts/ && bash ./build-project.sh "rebuild"

reset:
	cd ./scripts/ && bash ./reset-project.sh
