#!/bin/bash

##
# NGINX
##

# Purge logs

rm $NGINX_DIRECTORY_LOGS/*

# Feedback

echo 'Purge nginx logs ... done'

##
# APACHE
##

# Purge logs

rm $APACHE_DIRECTORY_LOGS/*

# Feedback

echo 'Purge apache logs ... done'

##
# PHP-FPM
##

# Purge logs

rm $PHP_DIRECTORY_LOGS/*

# Feedback

echo 'Purge php logs ... done'

##
# MARIADB
##

# Purge logs

rm $MARIADB_DIRECTORY_LOGS/*

# Feedback

echo 'Purge mariadb logs ... done'
