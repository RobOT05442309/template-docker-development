# Docker DPLMP (Docker - Proxy - Localhost - MariaDB - PHPMyAdmin)

This project is a LAMPP like stack for development purpose, you can customize this image to your needs in the .env file then create everything with using command describe in "Usage".

You can adapt the docker-compose.yml file to your needs if you wish to expose mariadb ports on 127.0.0.1, by default only nginx port 80 and 443 are bind to the host.

SSL is available if needed through nginx and apache.

Every container will run using the same user and group ID as the one you'll define in the .env file, note that with this configuration you can't bind port only accessible using root in each container. If you want to change this behavior, comment the "user" line define in each service of the docker-compose.yml file.

This project is stable at the time of writing. I needed an LAMPP like docker stack, that's why I made it, feel free to change whatever you want in this for your needs.

Some files might evolve with time.

You can choose your own password in the .env file if you want to, otherwise a random password will be generated during creation step.

## Services

- Proxy : Nginx container is used to redirect on apache.
- Localhost : Apache + PHP-FPM on one container, this is the one used for http://localhost and https://localhost
- MariaDB : MariaDB container for MySQL database.
- PHPMyAdmin : Apache + PHP-FPM + PHPMyadmin on one container, used to access MariaDB access it using http://phpmyadmin.localhost or https://phpmyadmin.localhost

## Dependencies

You'll need docker and docker-compose install on your system before running anything.

Your user need to be in "docker" group to run commands without sudo commands

```bash
usermod -a -G docker $USER
```

Reboot you system or logout to apply changes.

## Install

In order to install everything, you can just run the script on the location you want to setup your lampp stack, all file and folder will be created based on what you defined in the .env file.

## Environment

You can setup manually or let the script does the job for you in this part, if you want to make a change to the running container after the creation, just run :

```bash
docker-compose down && docker-compose up -d
```

To find your current user id and group id use this commands :

User ID
```bash
id -u $USER
```

Group ID
```bash
id -g $USER
```

Replace in the .env file :

```bash
# Containers IDs

USER_ID=1000 # Change this value for your User ID
GROUP_ID=1000 # Change this value for your Group ID
```

## Usage

- Once you've setup everything, you can run the following command :

```bash
make create
```

- To clean logs, you can use :

```bash
make clean
```

- You can rebuild each images if you make some changes, in order to do so, run :

```bash
make rebuild
```

- In order to purge folders, containers, images you can use :

```bash
make purge
```

Note : Purge DO NOT remove the "sources", "certificates", "databases" folders.

- If you wish to reset everything to default, use :

```bash
make reset
```

Note : Reset will delete every config file then recreate them, if you apply some changes, backup everything in a safe location then launch the reset command.

- In order to destroy everything, folders, containers, images, databases, you can use :

```bash
make destroy
```

Note : Destroy DO NOT remove the "sources" folder, this is the root of your website and project.

## Limitations

Once the container is running, you'll not be able to install anything using apt-get install. This limitation is due to the fact that the user running the container is www-data or mysql, if you want to change this, comment or remove the "user:" line in each docker services in the docker-compose.yml file.

## Customization

If you want to change default theme in PHPMyAdmin, two of them are available, you can defined it in the .env file :

```bash
PHPMYADMIN_THEME_DEFAULT=metro
```

Note : available themes are "metro" and "fallen"

## Notes

- If you launch the command to destroy everything, docker-compose file and folder structure will remain, you can remove everything manually or launch the command to create again, if auto generated file exists when running this command, script will not create them once again. This has been made this way to prevent any data loss if you change some config file or whatever.

- You need to use the same data directory for apache and php container, otherwise, you'll get a nice "file not found" when trying to read php files.

- In phpmyadmin first run, you can click on the bottom of the welcome screen to create phpmyadmin database if needed.

- If you want to use other theme for phpmyadmin, uncomment the line in the phpmyadmin-sql service to do so then copy paste each theme you want to add, this will override default available theme in container.

- If you make usage of user namespace (https://docs.docker.com/engine/security/userns-remap/) to run docker, this project might not work as expected, as stated in the known limitation : external (volume or storage) drivers which are unaware or incapable of using daemon user mappings. In my case, after few testing, I wasn't able to bind containers volumes properly, I've decided to run container as current user instead of trying to change some ownership on system folders used by docker.

- If you are using openvpn while starting the project, containers may failed to start (https://stackoverflow.com/questions/43720339/docker-error-could-not-find-an-available-non-overlapping-ipv4-address-pool-am). 

## Commands

Sample of docker and docker-compose commands

- Enter in container shell (i.e: Enter in container named apache-container) :

```bash
docker exec -ti apache-container bash
```

- Remove all non running container and all images, network, ...

```bash
docker system prune -a
```

- Build and run all container in docker compose file :

```bash
docker-compose up -d --build
```

- Start all container in docker compose file :

```bash
docker-compose up -d
```

- Start specific (php and mariadb) container in docker-compose file :

```bash
docker-compose up -d php mariadb
```

- Stop and delete all running container from docker-compose file :

```bash
docker-compose down
```

- Get system usage of a container (i.e. : check cpu usage of container named apache-container):

```bash
docker stats apache-container
```

## Changes

### Fixes

- Add limits for CPU and MEM usage into docker-compose.yml file.
- Patch bug where .env file couldn't be found using "source ./.env" when running "make create" in Ubuntu. 
- Change "sh" for "bash" in makefile, enhanced nginx default configuration file.
- Get user id from current user then write to .env if value is empty.
- Generate Blowfish secret automatically and write it in .env if value is empty.

### Improvements

- Not here yet.

