#!/bin/bash

##
# NGINX
##

# Create log : localhost-access.log

FILE='localhost-access.log'

if [ ! -f "$NGINX_DIRECTORY_LOGS/$FILE" ]
then

touch $NGINX_DIRECTORY_LOGS/$FILE

fi

# Create log : localhost-error.log

FILE='localhost-error.log'

if [ ! -f "$NGINX_DIRECTORY_LOGS/$FILE" ]
then

touch $NGINX_DIRECTORY_LOGS/$FILE

fi

# Feedback

echo 'Create nginx logs ... done'

##
# APACHE
##

# Create log : localhost-access.log

FILE='localhost-access.log'

if [ ! -f "$APACHE_DIRECTORY_LOGS/$FILE" ]
then

touch $APACHE_DIRECTORY_LOGS/$FILE

fi

# Create log : localhost-error.log

FILE='localhost-error.log'

if [ ! -f "$APACHE_DIRECTORY_LOGS/$FILE" ]
then

touch $APACHE_DIRECTORY_LOGS/$FILE

fi

# Create log : phpmyadmin-access.log

FILE='phpmyadmin-access.log'

if [ ! -f "$APACHE_DIRECTORY_LOGS/$FILE" ]
then

touch $APACHE_DIRECTORY_LOGS/$FILE

fi

# Create log : phpmyadmin-error.log

FILE='phpmyadmin-error.log'

if [ ! -f "$APACHE_DIRECTORY_LOGS/$FILE" ]
then

touch $APACHE_DIRECTORY_LOGS/$FILE

fi

# Feedback

echo 'Create apache logs ... done'

##
# PHP-FPM
##

# Create log : localhost-access.log

FILE='localhost-access.log'

if [ ! -f "$PHP_DIRECTORY_LOGS/$FILE" ]
then

touch $PHP_DIRECTORY_LOGS/$FILE

fi

# Create log : localhost-error.log

FILE='localhost-error.log'

if [ ! -f "$PHP_DIRECTORY_LOGS/$FILE" ]
then

touch $PHP_DIRECTORY_LOGS/$FILE

fi

# Create log : phpmyadmin-access.log

FILE='phpmyadmin-access.log'

if [ ! -f "$PHP_DIRECTORY_LOGS/$FILE" ]
then

touch $PHP_DIRECTORY_LOGS/$FILE

fi

# Create log : phpmyadmin-error.log

FILE='phpmyadmin-error.log'

if [ ! -f "$PHP_DIRECTORY_LOGS/$FILE" ]
then

touch $PHP_DIRECTORY_LOGS/$FILE

fi

# Feedback

echo 'Create php logs ... done'


##
# MARIADB
##

# Create log : localhost-error.log

FILE='localhost-error.log'

if [ ! -f "$MARIADB_DIRECTORY_LOGS/$FILE" ]
then

touch $MARIADB_DIRECTORY_LOGS/$FILE

fi

# Feedback

echo 'Create mariadb logs ... done'