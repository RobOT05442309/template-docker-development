#!/bin/bash

##
# DOCKER-COMPOSE
##

# Create file docker-compose.yml

FILE='docker-compose.yml'
VERSION="'2.4'"

if [ ! -f "$DOCKER_DIRECTORY_PROJECT/$FILE" ]
then

touch $DOCKER_DIRECTORY_PROJECT/$FILE

echo 'version: '$VERSION'

services:

   proxy:
     user: "${USER_ID}:${GROUP_ID}"
     container_name: "${NGINX_CONTAINER}"
     cpu_percent: '${NGINX_LIMIT_CPU}'
     mem_limit: '${NGINX_LIMIT_MEM}'
     memswap_limit: '${NGINX_LIMIT_SWAP}'
     build:
       context: .
       dockerfile: "./images/docker-proxy"
       args:
         USER_ID: "${USER_ID}"
         GROUP_ID: "${GROUP_ID}"
         TIMEZONE: "${TIMEZONE}"
         CERTIFICATE: "${SSL_CERT_NAME}"
         PORT_HTTP: "${NGINX_PORT_DOCK_HTTP}"
         PORT_HTTPS: "${NGINX_PORT_DOCK_HTTPS}"
     image: proxy:focal
     restart: unless-stopped
     volumes:
       - "${SSL_DIRECTORY_LIVE}:/etc/letsencrypt/live:ro"
       - "${SSL_DIRECTORY_LOCAL}:/etc/letsencrypt/local:ro"
       - "${NGINX_DIRECTORY_CONFIGS}/nginx.conf:/etc/nginx/conf.d/www.conf:ro"
       - "${NGINX_DIRECTORY_HOSTS}:/etc/nginx/sites-available:ro"
       - "${NGINX_DIRECTORY_HOSTS}:/etc/nginx/sites-enabled:ro"
       - "${NGINX_DIRECTORY_LOGS}/localhost-access.log:/var/log/nginx/access.log:rw"
       - "${NGINX_DIRECTORY_LOGS}/localhost-error.log:/var/log/nginx/error.log:rw"
       - "${PROJECT_DIRECTORY_SOURCES}:/var/www/html:rw"
     ports:
       #- "HOST:DOCKER"
       - "'${NGINX_PORT_HOST_HTTP}':'${NGINX_PORT_DOCK_HTTP}'"
       - "'${NGINX_PORT_HOST_HTTPS}':'${NGINX_PORT_DOCK_HTTPS}'"
     networks:
       - frontend
     links:
       - localhost
       - phpmyadmin

   localhost:
     user: "${USER_ID}:${GROUP_ID}"
     container_name: "${APACHE_PHP_CONTAINER}"
     cpu_percent: '${APACHE_PHP_LIMIT_CPU}'
     mem_limit: '${APACHE_PHP_LIMIT_MEM}'
     memswap_limit: '${APACHE_PHP_LIMIT_SWAP}'
     build:
       context: .
       dockerfile: "./images/docker-localhost"
       args:
         USER_ID: "${USER_ID}"
         GROUP_ID: "${GROUP_ID}"
         TIMEZONE: "${TIMEZONE}"
         PORT_HTTP: "${APACHE_PORT_DOCK_HTTP}"
         PORT_HTTPS: "${APACHE_PORT_DOCK_HTTPS}"
     image: localhost:focal
     restart: unless-stopped
     volumes:
       - "${SSL_DIRECTORY_LIVE}:/etc/letsencrypt/live:ro"
       - "${SSL_DIRECTORY_LOCAL}:/etc/letsencrypt/local:ro"
       - "${APACHE_DIRECTORY_CONFIGS}/apache.conf:/etc/apache2/conf-enabled/www.conf:ro"
       - "${PHP_DIRECTORY_CONFIGS}/php.conf:/etc/php/7.4/fpm/pool.d/90-phpfpm.conf:ro"
       - "${PHP_DIRECTORY_CONFIGS}/php.ini:/etc/php/7.4/fpm/conf.d/90-phpfpm.ini:ro"
       - "${APACHE_DIRECTORY_HOSTS}:/etc/apache2/sites-available:ro"
       - "${APACHE_DIRECTORY_HOSTS}:/etc/apache2/sites-enabled:ro"
       - "${APACHE_DIRECTORY_LOGS}/localhost-access.log:/var/log/apache2/access.log:rw"
       - "${APACHE_DIRECTORY_LOGS}/localhost-error.log:/var/log/apache2/error.log:rw"
       - "${PHP_DIRECTORY_LOGS}/localhost-access.log:/var/log/php7/access.log:rw"
       - "${PHP_DIRECTORY_LOGS}/localhost-error.log:/var/log/php7/error.log:rw"
       - "${PROJECT_DIRECTORY_SOURCES}:/var/www/html:rw"
     networks:
       - frontend
       - backend

   mariadb:
     user: "${USER_ID}:${GROUP_ID}"
     container_name: "${MARIADB_CONTAINER}"
     cpu_percent: '${MARIADB_LIMIT_CPU}'
     mem_limit: '${MARIADB_LIMIT_MEM}'
     memswap_limit: '${MARIADB_LIMIT_SWAP}'
     build:
       context: .
       dockerfile: "./images/docker-mariadb"
       args:
         USER_ID: ${USER_ID}
         GROUP_ID: ${GROUP_ID}
         TIMEZONE: ${TIMEZONE}
     image: mariadb:focal
     restart: unless-stopped
     environment:
         MYSQL_INITDB_SKIP_TZINFO: "1"
         MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
     volumes:
       - "${MARIADB_DIRECTORY_CONFIGS}:/etc/mysql/my.cnf.d:ro"
       - "${MARIADB_DIRECTORY_LOGS}/localhost-error.log:/var/log/mysql/error.log:rw"
       - "${MARIADB_DIRECTORY_DATABASES}:/var/lib/mysql:rw"
     #ports:
       #- "HOST:DOCKER"
       #- "'${MARIADB_PORT_HOST_SQL}':'${MARIADB_PORT_DOCK_SQL}'"
     networks:
       - backend

   phpmyadmin:
     user: "${USER_ID}:${GROUP_ID}"
     container_name: "${PHPMYADMIN_CONTAINER}"
     cpu_percent: '${PHPMYADMIN_LIMIT_CPU}'
     mem_limit: '${PHPMYADMIN_LIMIT_MEM}'
     memswap_limit: '${PHPMYADMIN_LIMIT_SWAP}'
     build:
       context: .
       dockerfile: "./images/docker-phpmyadmin"
       args:
         USER_ID: "${USER_ID}"
         GROUP_ID: "${GROUP_ID}"
         TIMEZONE: "${TIMEZONE}"
         PORT_HTTP: "${APACHE_PORT_DOCK_HTTP}"
         PORT_HTTPS: "${APACHE_PORT_DOCK_HTTPS}"
     image: phpmyadmin:focal
     restart: unless-stopped
     volumes:
       - "${SSL_DIRECTORY_LIVE}:/etc/letsencrypt/live:ro"
       - "${SSL_DIRECTORY_LOCAL}:/etc/letsencrypt/local:ro"
       - "${APACHE_DIRECTORY_CONFIGS}/apache.conf:/etc/apache2/conf-enabled/www.conf:ro"
       - "${PHP_DIRECTORY_CONFIGS}/php.conf:/etc/php/7.4/fpm/pool.d/90-phpfpm.conf:ro"
       - "${PHP_DIRECTORY_CONFIGS}/php.ini:/etc/php/7.4/fpm/conf.d/90-phpfpm.ini:ro"
       - "${PHPMYADMIN_DIRECTORY_CONFIGS}/config.db.php:/etc/phpmyadmin/config-db.php:ro"
       - "${PHPMYADMIN_DIRECTORY_CONFIGS}/config.inc.php:/etc/phpmyadmin/conf.d/config-usr.php:ro"
       - "${PHPMYADMIN_DIRECTORY_HOSTS}:/etc/apache2/sites-available:ro"
       - "${PHPMYADMIN_DIRECTORY_HOSTS}:/etc/apache2/sites-enabled:ro"
       - "${APACHE_DIRECTORY_LOGS}/phpmyadmin-access.log:/var/log/apache2/access.log:rw"
       - "${APACHE_DIRECTORY_LOGS}/phpmyadmin-error.log:/var/log/apache2/error.log:rw"
       - "${PHP_DIRECTORY_LOGS}/phpmyadmin-access.log:/var/log/php7/access.log:rw"
       - "${PHP_DIRECTORY_LOGS}/phpmyadmin-error.log:/var/log/php7/error.log:rw"
       #- "${PHPMYADMIN_DIRECTORY_THEMES}/fallen:/usr/share/phpmyadmin/themes"
     networks:
       - frontend
       - backend
     links:
       - mariadb
     depends_on:
       - mariadb

networks:

  frontend:
    name: "${DOCKER_NETWORK_FRONTEND}"
  backend:
    name: "${DOCKER_NETWORK_BACKEND}"
' >> $DOCKER_DIRECTORY_PROJECT/$FILE

fi

# Feedback

echo 'Create docker-compose.yml ... done'
