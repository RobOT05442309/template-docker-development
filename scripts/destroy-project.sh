#!/bin/bash

##
# PROJECT LOCATION
##

DOCKER_DIRECTORY_PROJECT="$( echo $(cd ../ && pwd) )"
cd $DOCKER_DIRECTORY_PROJECT

##
# IMPORT ENVIRONMENT VARIABLES
##

source $DOCKER_DIRECTORY_PROJECT/.env

##
# STOP DOCKER
##

. $DOCKER_DIRECTORY_PROJECT/scripts/stop/stop-docker-project.sh

##
# PURGE SYSTEM
##

. $DOCKER_DIRECTORY_PROJECT/scripts/purge/purge-docker-system.sh

##
# DESTROY DIRECTORIES
##

. $DOCKER_DIRECTORY_PROJECT/scripts/destroy/destroy-project-directory.sh
. $DOCKER_DIRECTORY_PROJECT/scripts/destroy/destroy-certificates-directory.sh


##
# DESTROY DATABASES
##

. $DOCKER_DIRECTORY_PROJECT/scripts/destroy/destroy-project-databases.sh

##
# DESTROY DOCKER-COMPOSE
##

. $DOCKER_DIRECTORY_PROJECT/scripts/destroy/destroy-docker-compose.sh

