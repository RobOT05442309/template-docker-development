#!/bin/bash

##
# PROJECT
##

# Create file : infos.php

FILE='infos.php'

if [ ! -f "$PROJECT_DIRECTORY_SOURCES/$FILE" ]
then

touch $PROJECT_DIRECTORY_SOURCES/$FILE

echo '<?php phpinfo(); ?>
' >> $PROJECT_DIRECTORY_SOURCES/$FILE

fi

# Create file : mysql.php

FILE='mysql.php'

if [ ! -f "$PROJECT_DIRECTORY_SOURCES/$FILE" ]
then

touch $PROJECT_DIRECTORY_SOURCES/$FILE

echo '<?php

// Message

echo "Connect test to MariaDB<br><br>";

// Variables

$host = "'$MARIADB_CONTAINER'";
$port= "'$MARIADB_PORT_DOCK_SQL'";
$username = "root";
$password = "'$MYSQL_ROOT_PASSWORD'";
$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");

// Connection

try {
	$connect = new PDO("mysql:host=".$host.";port=".$port, $username, $password);
}
catch(PDOException $exception) {
	die("Error : " . $exception->getMessage());
}

// Databases

$databases = $connect->query( "SHOW DATABASES" );
while( ( $database = $databases->fetchColumn( 0 ) ) !== false ) {
	echo $database."<br>";
}

?>
' >> $PROJECT_DIRECTORY_SOURCES/$FILE

fi

# Feedback

echo 'Create testing files ... done'

##
# SSL
##

# Create certificates

FILE='Docker_Selfsigned.key'

if [ ! -f "$SSL_DIRECTORY_LOCAL/$FILE" ]
then

openssl req -x509 -nodes -days 36500 -newkey rsa:2048 -subj "/C=$SSL_CERT_COUNTRY/ST=$SSL_CERT_STATE/L=$SSL_CERT_CITY/O=Docker/CN=Docker Localhost" -keyout $SSL_DIRECTORY_LOCAL/$SSL_CERT_NAME.key -out $SSL_DIRECTORY_LOCAL/$SSL_CERT_NAME.pem &> /dev/null

fi

# Feedback

echo 'Create ssl certificates ... done'

##
# NGINX
##

# Create file : nginx.conf

FILE='nginx.conf'

if [ ! -f "$NGINX_DIRECTORY_CONFIGS/$FILE" ]
then

touch $NGINX_DIRECTORY_CONFIGS/$FILE

echo '# This file override directives from /etc/nginx/nginx.conf

# Server token

server_tokens off;
client_header_timeout 1296000s;
client_body_timeout 1296000s;
proxy_read_timeout 1296000s;
client_max_body_size 2048M;
' >> $NGINX_DIRECTORY_CONFIGS/$FILE

fi

# Create file : localhost.conf

FILE='localhost.conf'

if [ ! -f "$NGINX_DIRECTORY_HOSTS/$FILE" ]
then

touch $NGINX_DIRECTORY_HOSTS/$FILE

echo '# Port '$NGINX_PORT_DOCK_HTTP' : localhost

server {

    # Listening Port

    listen '$NGINX_PORT_DOCK_HTTP';

    # Server name

    server_name localhost;

    # Force redirect to SSL (uncomment line then comment proxy redirect since it will not be used anymore)
    
    #return 301 https://$server_name$request_uri; 
    
    # Proxy redirect for localhost

    location / {
        proxy_pass http://'$APACHE_PHP_CONTAINER':'$APACHE_PORT_DOCK_HTTP';
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        expires off;
    }
    
    # Protect htaccess/htpassword files

    location ~ /\.ht {
        deny all;
    }

    # Define default logs
    
    error_log  /var/log/nginx/error.log;
    access_log  /var/log/nginx/access.log;
    
}

# Port '$NGINX_PORT_DOCK_HTTPS' : localhost

server {

    # Listening Port

    listen '$NGINX_PORT_DOCK_HTTPS' ssl;
    
    # Server name

    server_name localhost;

    # SSL certificates

    ssl_certificate /etc/letsencrypt/local/'$SSL_CERT_NAME'.pem;
    ssl_certificate_key /etc/letsencrypt/local/'$SSL_CERT_NAME'.key;

    # Proxy redirect for localhost

    location / {
        proxy_pass https://'$APACHE_PHP_CONTAINER':'$APACHE_PORT_DOCK_HTTPS';
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        expires off;
    }
    
    # Protect htaccess/htpassword files

    location ~ /\.ht {
        deny all;
    }

    # Define default logs
    
    error_log  /var/log/nginx/error.log;
    access_log  /var/log/nginx/access.log;
    
}
' >> $NGINX_DIRECTORY_HOSTS/$FILE

fi

# Create file : phpmyadmin.conf

FILE='phpmyadmin.conf'

if [ ! -f "$NGINX_DIRECTORY_HOSTS/$FILE" ]
then

touch $NGINX_DIRECTORY_HOSTS/$FILE

echo '# Port '$NGINX_PORT_DOCK_HTTP' : phpmyadmin.localhost

server {

    # Listening Port

    listen '$NGINX_PORT_DOCK_HTTP';

    # Server name

    server_name phpmyadmin.localhost;
    
    # Force redirect to SSL (uncomment line then comment proxy redirect since it will not be used anymore)

    #return 301 https://$server_name$request_uri; 

    # Proxy redirect for phpmyadmin.localhost

    location / {
        proxy_pass http://'$PHPMYADMIN_CONTAINER':'$APACHE_PORT_DOCK_HTTP';
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        expires off;
    }
    
    # Protect htaccess/htpassword files

    location ~ /\.ht {
        deny all;
    }

    # Define default logs
    
    error_log  /var/log/nginx/error.log;
    access_log  /var/log/nginx/access.log;
    
}

# Port '$NGINX_PORT_DOCK_HTTPS' : phpmyadmin.localhost

server {

    # Listening Port

    listen '$NGINX_PORT_DOCK_HTTPS';
    
    # Server name

    server_name phpmyadmin.localhost;

    # SSL certificates

    ssl_certificate /etc/letsencrypt/local/'$SSL_CERT_NAME'.pem;
    ssl_certificate_key /etc/letsencrypt/local/'$SSL_CERT_NAME'.key;

    # Proxy redirect for phpmyadmin.localhost

    location / {
        proxy_pass https://'$PHPMYADMIN_CONTAINER':'$APACHE_PORT_DOCK_HTTPS';
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        expires off;
    }
    
    # Protect htaccess/htpassword files

    location ~ /\.ht {
        deny all;
    }

    # Define default logs
    
    error_log  /var/log/nginx/error.log;
    access_log  /var/log/nginx/access.log;
    
}
' >> $NGINX_DIRECTORY_HOSTS/$FILE

fi

# Feedback

echo 'Create nginx files ... done'

##
# APACHE
##

# Create file : apache.conf

FILE='apache.conf'

if [ ! -f "$APACHE_DIRECTORY_CONFIGS/$FILE" ]
then

touch $APACHE_DIRECTORY_CONFIGS/$FILE

echo '# This file override directives from /etc/apache2/apache2.conf

# Server signature

ServerSignature Off

# Extended status

ExtendedStatus off

# Server token

ServerTokens Prod

# Trace

TraceEnable off

# File ETag

FileETag none

# Timeout

Timeout 3000
' >> $APACHE_DIRECTORY_CONFIGS/$FILE

fi

# Create file : localhost.conf

FILE='localhost.conf'

if [ ! -f "$APACHE_DIRECTORY_HOSTS/$FILE" ]
then

touch $APACHE_DIRECTORY_HOSTS/$FILE

echo '# Port '$APACHE_PORT_DOCK_HTTP' : localhost

<VirtualHost *:'$APACHE_PORT_DOCK_HTTP'>

    # Server name

    ServerName localhost

	# Define base directory

    DocumentRoot /var/www/html
    
    # Define headers options
    
    <IfModule mod_headers.c>
        Header always set Referrer-Policy "no-referrer"
        Header always set X-Content-Type-Options "nosniff"
        Header always set X-FRAME-OPTIONS "DENY"
        Header always set X-XSS-Protection "1; mode=block"
    </IfModule>
    
    # Protect htaccess/htpassword files
    
    <FilesMatch "^\.ht">
        Order allow,deny
        Deny from all
    </FilesMatch>

    # Configuration for directory
    
    <Directory /var/www/html>
	    Options -Indexes
	    Options +FollowSymlinks
        AllowOverride All
        Require all granted
    </Directory>

    # Configuration for PHP files
    
    <FilesMatch \.php$>
        #SetHandler "proxy:fcgi://'$APACHE_PHP_CONTAINER':'$PHP_PORT_DOCK_FPM'"
        #SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost:'$PHP_PORT_DOCK_FPM'"
        SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost"
    </FilesMatch>

    # Define default logs
    
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

# Port '$APACHE_PORT_DOCK_HTTPS' : localhost

<IfModule mod_ssl.c>

<VirtualHost *:'$APACHE_PORT_DOCK_HTTPS'>

    # Server name

    ServerName localhost

	# Define base directory

    DocumentRoot /var/www/html
    
    # Define SSL certificates
    
    SSLEngine on
    SSLCertificateFile /etc/letsencrypt/local/'$SSL_CERT_NAME'.pem
    SSLCertificateKeyFile /etc/letsencrypt/local/'$SSL_CERT_NAME'.key
    
    # Define headers options
    
    <IfModule mod_headers.c>
        Header always set Referrer-Policy "no-referrer"
        Header always set X-Content-Type-Options "nosniff"
        Header always set X-FRAME-OPTIONS "DENY"
        Header always set X-XSS-Protection "1; mode=block"
        Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"
    </IfModule>
    
    # Protect htaccess/htpassword files
    
    <FilesMatch "^\.ht">
        Order allow,deny
        Deny from all
    </FilesMatch>

    # Configuration for directory
    
    <Directory /var/www/html>
	    Options -Indexes
	    Options +FollowSymlinks
        AllowOverride All
        Require all granted
    </Directory>

    # Configuration for PHP files
    
    <FilesMatch \.php$>
        #SetHandler "proxy:fcgi://'$APACHE_PHP_CONTAINER':'$PHP_PORT_DOCK_FPM'"
        #SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost:'$PHP_PORT_DOCK_FPM'"
        SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost"
    </FilesMatch>

    # Define default logs
    
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

</IfModule>
' >> $APACHE_DIRECTORY_HOSTS/$FILE

fi

# Feedback

echo 'Create apache files ... done'


##
# PHP-FPM
##

# Create file : php.conf

FILE='php.conf'

if [ ! -f "$PHP_DIRECTORY_CONFIGS/$FILE" ]
then

touch $PHP_DIRECTORY_CONFIGS/$FILE

echo '; This file override directives from /etc/php/7.4/fpm/php.conf and any directives from /etc/php/7.4/fpm/pool.d/*.conf

[global]

; ErrorLog
error_log = /var/log/php7/error.log

[www]

; Logs 
access.log = /var/log/php7/access.log

; Unix user 
; user = www-data 
; group = www-data 

; FastCGI address 
;listen = 9001

; FastCGI user 
; listen.owner = www-data 
; listen.group = www-data

; Control of child processes 
; pm = dynamic 
; pm.max_children = 5 
; pm.start_servers = 2 
; pm.min_spare_servers = 1 
; pm.max_spare_servers = 3
' >> $PHP_DIRECTORY_CONFIGS/$FILE

fi

# Create file : php.ini

FILE='php.ini'

if [ ! -f "$PHP_DIRECTORY_CONFIGS/$FILE" ]
then

touch $PHP_DIRECTORY_CONFIGS/$FILE

echo '; This file override directives from /etc/php/7.4/fpm/php.ini

; Memory

memory_limit = 2048M

; Time

max_execution_time = 1296000
max_input_time = 1296000

; Post

post_max_size = 2048M

; Upload

upload_max_filesize = 2048M
max_file_uploads = 100

; Session

session.gc_maxlifetime = 28800

; Errors

display_errors = On
track_errors = Off

; Logs

log_errors = On
' >> $PHP_DIRECTORY_CONFIGS/$FILE

fi

# Feedback

echo 'Create php files ... done'


##
# MARIADB
##

# Create file : mariadb.cli.cnf

FILE='mysql.cli.cnf'

if [ ! -f "$MARIADB_DIRECTORY_CONFIGS/$FILE" ]
then

touch $MARIADB_DIRECTORY_CONFIGS/$FILE

echo "# This file override directives for mysql (include in /etc/mysql/my.cnf.d/*.cnf)

[client]

# CHARACTER & COLLATE

default-character-set="$MYSQL_COLLATION"

[mysql]

# TIMEZONE

default_time_zone="$TIMEZONE"

# CHARACTER & COLLATE

default-character-set="$MYSQL_COLLATION"

[mysqld]

# CHARACTER & COLLATE

collation-server="$MYSQL_COLLATION"
init-connect='SET NAMES "$MYSQL_CHARACTER"'
character-set-server="$MYSQL_CHARACTER"

# SAFETY

max-allowed-packet=100M
#max-connect-errors=1000000
#sysdate-is-now=1
#innodb=FORCE
#innodb-strict-mode=1

# CACHES & LIMITS

#tmp-table-size=32M
#max-heap-table-size=32M
#query-cache-type=0
#query-cache-size=0
#max-connections=500
#thread-cache-size=50
#open-files-limit=65535
#table-definition-cache=4096
#table-open-cache=10240

# LOGS

log-error=/var/log/mysql/error.log
#log-queries-not-using-indexes=1
" >> $MARIADB_DIRECTORY_CONFIGS/$FILE

fi

# Create file : mariadb.dump.cnf

FILE='mysql.dump.cnf'

if [ ! -f "$MARIADB_DIRECTORY_CONFIGS/$FILE" ]
then

touch $MARIADB_DIRECTORY_CONFIGS/$FILE

echo '# This file set directives for mysql-dump (include in /etc/mysql/my.cnf.d/*.cnf)

[mysqldump]

# User

user=root

# Password

password="'$MYSQL_ROOT_PASSWORD'"

# Host

host=localhost
' >> $MARIADB_DIRECTORY_CONFIGS/$FILE

fi

# Feedback

echo 'Create mariadb files ... done'


##
# PHPMYADMIN
##

# Create file : config.db.php

FILE='config.db.php'

if [ ! -f "$PHPMYADMIN_DIRECTORY_CONFIGS/$FILE" ]
then

touch $PHPMYADMIN_DIRECTORY_CONFIGS/$FILE

echo "<?php // This file override directives from /etc/phpmyadmin/config-db.php

/* Database user */

\$dbuser='root';

/* Database password */

\$dbpass='"$MYSQL_ROOT_PASSWORD"';

/* Database path */

\$basepath='';

/* Database phpmyadmin */

\$dbname='phpmyadmin';

/* Server hostname */

\$dbserver='"$MARIADB_CONTAINER"';

/* Server port */

\$dbport='"$MARIADB_PORT_DOCK_SQL"';

/* Database type */

\$dbtype='mysql';
" >> $PHPMYADMIN_DIRECTORY_CONFIGS/$FILE

fi

# Create file : config.inc.php

FILE='config.inc.php'

if [ ! -f "$PHPMYADMIN_DIRECTORY_CONFIGS/$FILE" ]
then

touch $PHPMYADMIN_DIRECTORY_CONFIGS/$FILE

echo "<?php // This file override directives from /etc/phpmyadmin/config.inc.php

/* Secret */

\$cfg['blowfish_secret'] = '"$PHPMYADMIN_SECRET_BLOWFISH"';

/* Time limit */

\$cfg['ExecTimeLimit'] = 30000;

/* Cookie */

\$cfg['LoginCookieValidity'] = '28800';

/* Theme */

\$cfg['ThemeDefault'] = '"$PHPMYADMIN_THEME_DEFAULT"';
" >> $PHPMYADMIN_DIRECTORY_CONFIGS/$FILE

fi

# Create file : localhost.conf

FILE='localhost.conf'

if [ ! -f "$PHPMYADMIN_DIRECTORY_HOSTS/$FILE" ]
then

touch $PHPMYADMIN_DIRECTORY_HOSTS/$FILE

echo '# Port '$APACHE_PORT_DOCK_HTTP' : localhost

<VirtualHost *:'$APACHE_PORT_DOCK_HTTP'>

    # Server name

    ServerName localhost

	# Define base directory

    DocumentRoot /usr/share/phpmyadmin
    
    # Define headers options
    
    <IfModule mod_headers.c>
        Header always set Referrer-Policy "no-referrer"
        Header always set X-Content-Type-Options "nosniff"
        Header always set X-FRAME-OPTIONS "DENY"
        Header always set X-XSS-Protection "1; mode=block"
    </IfModule>
    
    # Protect htaccess/htpassword files
    
    <FilesMatch "^\.ht">
        Order allow,deny
        Deny from all
    </FilesMatch>

    # Configuration for PHP files
    
    <FilesMatch \.php$>
        #SetHandler "proxy:fcgi://'$PHPMYADMIN_CONTAINER':'$PHP_PORT_DOCK_FPM'"
        #SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost:'$PHP_PORT_DOCK_FPM'"
        SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost"
    </FilesMatch>

    # Define default logs
    
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

# Port '$APACHE_PORT_DOCK_HTTPS' : localhost

<IfModule mod_ssl.c>

<VirtualHost *:'$APACHE_PORT_DOCK_HTTPS'>

    # Server name

    ServerName localhost

	# Define base directory

    DocumentRoot /usr/share/phpmyadmin
    
    # Define SSL certificates
    
    SSLEngine on
    SSLCertificateFile /etc/letsencrypt/local/'$SSL_CERT_NAME'.pem
    SSLCertificateKeyFile /etc/letsencrypt/local/'$SSL_CERT_NAME'.key
    
    # Define headers options
    
    <IfModule mod_headers.c>
        Header always set Referrer-Policy "no-referrer"
        Header always set X-Content-Type-Options "nosniff"
        Header always set X-FRAME-OPTIONS "DENY"
        Header always set X-XSS-Protection "1; mode=block"
        Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"
    </IfModule>
    
    # Protect htaccess/htpassword files
    
    <FilesMatch "^\.ht">
        Order allow,deny
        Deny from all
    </FilesMatch>

    # Configuration for PHP files
    
    <FilesMatch \.php$>
        #SetHandler "proxy:fcgi://'$PHPMYADMIN_CONTAINER':'$PHP_PORT_DOCK_FPM'"
        #SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost:'$PHP_PORT_DOCK_FPM'"
        SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost"
    </FilesMatch>

    # Define default logs
    
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

</IfModule>
' >> $PHPMYADMIN_DIRECTORY_HOSTS/$FILE

fi

# Feedback

echo 'Create phpmyadmin files ... done'
