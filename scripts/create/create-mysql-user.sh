#!/bin/bash

##
# DOCKER
##

docker exec -i $MARIADB_CONTAINER bash -c "mysql -u root -p$MYSQL_ROOT_PASSWORD -e \"CREATE USER '$MYSQL_USER_NAME'@'%' IDENTIFIED BY '$MYSQL_USER_PASSWORD';\""
docker exec -i $MARIADB_CONTAINER bash -c "mysql -u root -p$MYSQL_ROOT_PASSWORD -e \"GRANT ALL PRIVILEGES ON $MYSQL_DATABASE . * TO '$MYSQL_USER_NAME'@'%';\""
docker exec -i $MARIADB_CONTAINER bash -c "mysql -u root -p$MYSQL_ROOT_PASSWORD -e \"FLUSH PRIVILEGES;\""