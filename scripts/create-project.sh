#!/bin/bash

##
# PROJECT LOCATION
##

DOCKER_DIRECTORY_PROJECT="$( echo $(cd ../ && pwd) )"
cd $DOCKER_DIRECTORY_PROJECT

##
# GENERATE ENVIRONMENT VARIABLES
##

. $DOCKER_DIRECTORY_PROJECT/scripts/create/create-project-environment.sh

##
# IMPORT ENVIRONMENT VARIABLES
##

source $DOCKER_DIRECTORY_PROJECT/.env

##
# CREATE DIRECTORIES
##

. $DOCKER_DIRECTORY_PROJECT/scripts/create/create-project-directories.sh

##
# CREATE FILES
##

. $DOCKER_DIRECTORY_PROJECT/scripts/create/create-project-files.sh

##
# CREATE LOGS
##

. $DOCKER_DIRECTORY_PROJECT/scripts/create/create-project-logs.sh

##
# CREATE DOCKER-COMPOSE
##

. $DOCKER_DIRECTORY_PROJECT/scripts/create/create-docker-compose.sh

##
# BUILD IMAGES
##

. $DOCKER_DIRECTORY_PROJECT/scripts/build/build-docker-images.sh

##
# START DOCKER
##

. $DOCKER_DIRECTORY_PROJECT/scripts/start/start-docker-project.sh


