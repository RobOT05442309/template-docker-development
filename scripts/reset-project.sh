#!/bin/bash

##
# PROJECT LOCATION
##

DOCKER_DIRECTORY_PROJECT="$( echo $(cd ../ && pwd) )"
cd $DOCKER_DIRECTORY_PROJECT

##
# IMPORT ENVIRONMENT VARIABLES
##

source $DOCKER_DIRECTORY_PROJECT/.env

##
# STOP DOCKER
##

. $DOCKER_DIRECTORY_PROJECT/scripts/stop/stop-docker-project.sh

##
# PURGE LOGS
##

. $DOCKER_DIRECTORY_PROJECT/scripts/purge/purge-project-logs.sh

##
# CREATE LOGS
##

. $DOCKER_DIRECTORY_PROJECT/scripts/create/create-project-logs.sh

##
# PURGE FILES
##

. $DOCKER_DIRECTORY_PROJECT/scripts/purge/purge-project-files.sh

##
# CREATE FILES
##

. $DOCKER_DIRECTORY_PROJECT/scripts/create/create-project-files.sh

##
# START DOCKER
##

. $DOCKER_DIRECTORY_PROJECT/scripts/start/start-docker-project.sh


