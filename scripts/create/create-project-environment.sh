#!/bin/bash

##
# USER
##

USER_ID_EXIST="$(grep "\bUSER_ID=\b" $DOCKER_DIRECTORY_PROJECT/.env)"

if [[ -z "$USER_ID_EXIST" ]]
then

USER_ID="$(id -u)"

sed -i "s/USER_ID=/USER_ID=$USER_ID/" $DOCKER_DIRECTORY_PROJECT/.env

fi


GROUP_ID_EXIST="$(grep "\bGROUP_ID=\b" $DOCKER_DIRECTORY_PROJECT/.env)"

if [[ -z "$GROUP_ID_EXIST" ]]
then

GROUP_ID="$(id -g)"

sed -i "s/GROUP_ID=/GROUP_ID=$GROUP_ID/" $DOCKER_DIRECTORY_PROJECT/.env

fi

##
# MARIADB
##

MYSQL_ROOT_PASSWORD_EXIST="$(grep "\bMYSQL_ROOT_PASSWORD=\b" $DOCKER_DIRECTORY_PROJECT/.env)"

if [[ -z "$MYSQL_ROOT_PASSWORD_EXIST" ]]
then

MYSQL_ROOT_PASSWORD="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 25)"

sed -i "s/MYSQL_ROOT_PASSWORD=/MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD/" $DOCKER_DIRECTORY_PROJECT/.env

fi


##
# PHPMYADMIN
##

PHPMYADMIN_SECRET_BLOWFISH_EXIST="$(grep "\bPHPMYADMIN_SECRET_BLOWFISH=\b" $DOCKER_DIRECTORY_PROJECT/.env)"

if [[ -z "$PHPMYADMIN_SECRET_BLOWFISH_EXIST" ]]
then

PHPMYADMIN_SECRET_BLOWFISH="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 35)"

sed -i "s/PHPMYADMIN_SECRET_BLOWFISH=/PHPMYADMIN_SECRET_BLOWFISH=$PHPMYADMIN_SECRET_BLOWFISH/" $DOCKER_DIRECTORY_PROJECT/.env

fi


