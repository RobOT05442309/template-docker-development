#!/bin/bash

##
# PROJECT
##

# Create directories

mkdir -p $PROJECT_DIRECTORY_SOURCES
mkdir -p $PROJECT_DIRECTORY_DATABASES

# Feedback

echo 'Create project directories ... done'

##
# SSL
##

# Create directories

mkdir -p $SSL_DIRECTORY_LOCAL
mkdir -p $SSL_DIRECTORY_LIVE

# Feedback

echo 'Create certificates directories ... done'

##
# NGINX
##

# Create directories

mkdir -p $NGINX_DIRECTORY_CONFIGS
mkdir -p $NGINX_DIRECTORY_HOSTS
mkdir -p $NGINX_DIRECTORY_LOGS

# Feedback

echo 'Create nginx directories ... done'

##
# APACHE
##

# Create directories

mkdir -p $APACHE_DIRECTORY_CONFIGS
mkdir -p $APACHE_DIRECTORY_HOSTS
mkdir -p $APACHE_DIRECTORY_LOGS

# Feedback

echo 'Create apache directories ... done'

##
# PHP-FPM
##

# Create directories

mkdir -p $PHP_DIRECTORY_CONFIGS
mkdir -p $PHP_DIRECTORY_LOGS

# Feedback

echo 'Create php directories ... done'


##
# MARIADB
##

# Create directories

mkdir -p $MARIADB_DIRECTORY_CONFIGS
mkdir -p $MARIADB_DIRECTORY_DATABASES
mkdir -p $MARIADB_DIRECTORY_LOGS

# Feedback

echo 'Create mariadb directories ... done'


##
# PHPMYADMIN
##

# Create directories

mkdir -p $PHPMYADMIN_DIRECTORY_CONFIGS
mkdir -p $PHPMYADMIN_DIRECTORY_HOSTS
mkdir -p $PHPMYADMIN_DIRECTORY_THEMES

# Feedback

echo 'Create phpmyadmin directories ... done'
