#!/bin/bash

##
# PROJECT LOCATION
##

DOCKER_DIRECTORY_PROJECT="$( echo $(cd ../ && pwd) )"
cd $DOCKER_DIRECTORY_PROJECT

##
# ACTION TYPE
##

ACTION=$1

##
# STOP DOCKER
##

if [[ "$ACTION" == "rebuild" ]]
then

. $DOCKER_DIRECTORY_PROJECT/scripts/stop/stop-docker-project.sh

fi

##
# BUILD IMAGES
##

. $DOCKER_DIRECTORY_PROJECT/scripts/build/build-docker-images.sh

##
# START DOCKER
##

. $DOCKER_DIRECTORY_PROJECT/scripts/start/start-docker-project.sh